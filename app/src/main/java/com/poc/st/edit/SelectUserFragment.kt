package com.poc.st.edit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.poc.st.MainActivity
import com.poc.st.R
import com.poc.st.add.repository.db.UserPropertyDao
import kotlinx.android.synthetic.main.fragment_select_user.*
import org.koin.android.ext.android.inject

class SelectUserFragment : Fragment() {

    val userPropertyDao : UserPropertyDao by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_select_user, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val userList = userPropertyDao.getAllUsersWithProperties()
        if(userList.isEmpty()){

            txtSelectCustomer.text = "No user added yet!!"

        }else{

            listUsers.layoutManager = LinearLayoutManager(context)
            val adapter = SelectUserListAdapter(userList, onItemSelectedListener = {
                position ->
                (activity as MainActivity).switchFragment(EditPropertyFragment.newInstance(userList[position]))

            })
            listUsers.adapter = adapter
        }

    }

    companion object {
         @JvmStatic
        fun newInstance() = SelectUserFragment()
    }
}
