package com.poc.st.edit

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.poc.st.R
import com.poc.st.common.User
import com.poc.st.listen
import kotlinx.android.synthetic.main.user_list_item_lay.view.*

class SelectUserListAdapter(
    private val userList: List<User> = mutableListOf(),
    private val onItemSelectedListener: (position: Int) -> Unit
) :
    RecyclerView.Adapter<SelectUserListAdapter.UserViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        UserViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.select_user_list_item_row_lay,
                parent,
                false
            )
        )

    override fun getItemCount(): Int = userList.size

    override fun onBindViewHolder(holder: SelectUserListAdapter.UserViewHolder, position: Int) {
        holder.onBind(userList[position])
        holder.listen(onItemSelectedListener)
    }

    inner class UserViewHolder(var userView: View) : RecyclerView.ViewHolder(userView) {
        fun onBind(user: User) {
            userView.txtUserName.text = user.name
        }
    }
}