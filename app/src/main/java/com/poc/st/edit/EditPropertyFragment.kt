package com.poc.st.edit


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.poc.st.MainActivity
import com.poc.st.R
import com.poc.st.add.viewmodel.AddUserPropertyViewModel
import com.poc.st.common.User
import com.poc.st.showToast
import kotlinx.android.synthetic.main.fragment_edit_property.*
import org.koin.androidx.viewmodel.ext.android.viewModel

private const val USER = "user"

class EditPropertyFragment : Fragment() {

    private lateinit var user: User
    private val propertyViewModel: AddUserPropertyViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            user = it.getSerializable(USER) as User
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_edit_property, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        txtUser.text = "Editing the properties for the user : ${user.name}"
        searchEditText.initView(propertyViewModel)
        searchEditText.updateList(user.propertyList)
        propertyViewModel.getStatus().observe(this, Observer { status ->
            when (status) {
                AddUserPropertyViewModel.Status.ERROR_EMPTY_PROPERTY -> searchEditText.showErrorEntry("Mandatory to select property")
                AddUserPropertyViewModel.Status.SUCCESS_SEARCH_PROPERTY ->searchEditText.showSuccessEntry()
                AddUserPropertyViewModel.Status.CANCEL_CLICKED ->searchEditText.showNormalEntry()

            }
        })
        btnEditCancel.setOnClickListener {
            (activity as MainActivity).switchFragment(SelectUserFragment.newInstance())
        }

        btnEditOk.setOnClickListener {
            propertyViewModel.updateUserWithProperties(user, searchEditText.getPropertyList()){success ->
                activity?.let {
                    if (success) {
                        it.showToast("User updated successfully")
                        searchEditText.resetView()
                    }else{
                        it.showToast("Sorry. Unable to update the user")
                    }
                }
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(user: User) =
            EditPropertyFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(USER, user)
                }
            }
    }
}
