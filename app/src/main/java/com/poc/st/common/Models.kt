package com.poc.st.common

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.io.Serializable


@Entity(tableName = "user")
data class User(
    @PrimaryKey
    var name: String,
    @Ignore
    var propertyList: List<Property> = mutableListOf()) : Serializable{
    constructor() : this("")
}

@Entity(tableName = "property", primaryKeys = arrayOf("name","userName"))
data class Property(
    val name: String,
    var isSelected: Boolean = false,
    var userName: String = "Bend"
) : Serializable


