package com.poc.st

import android.app.Application
import com.poc.st.di.stModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class STApplication : Application(){

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@STApplication)
            modules(stModule)
        }

    }

}