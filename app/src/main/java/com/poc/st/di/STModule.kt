package com.poc.st.di

import androidx.room.Room
import com.poc.st.add.repository.UserPropertyRepository
import com.poc.st.add.repository.db.AppDatabase
import com.poc.st.add.repository.db.UserPropertyDao
import com.poc.st.add.viewmodel.AddUserPropertyViewModel
import com.poc.st.view.ViewUserViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module


val stModule : Module = module {

    viewModel { AddUserPropertyViewModel(get()) }
    viewModel { ViewUserViewModel(get()) }

    single { UserPropertyRepository(get())}
    single { UserPropertyDao(get(),get()) }

    single { Room.databaseBuilder(androidContext()
        ,AppDatabase::class.java,
        AppDatabase.DATABASE_NAME).allowMainThreadQueries().build() }
    single { get<AppDatabase>().userDao() }
    single { get<AppDatabase>().propertyDao()}


}

