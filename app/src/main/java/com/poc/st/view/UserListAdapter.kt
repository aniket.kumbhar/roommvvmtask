package com.poc.st.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.poc.st.R
import com.poc.st.common.User
import kotlinx.android.synthetic.main.user_list_item_lay.view.*

class UserListAdapter(private val userList: List<User> = mutableListOf()) :
    RecyclerView.Adapter<UserListAdapter.UserViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        UserViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.user_list_item_lay,
                parent,
                false
            )
        )

    override fun getItemCount(): Int = userList.size

    override fun onBindViewHolder(holder: UserListAdapter.UserViewHolder, position: Int) {
        holder.onBind(userList[position])
    }

    inner class UserViewHolder(var userView: View) : RecyclerView.ViewHolder(userView) {
        fun onBind(user: User) {

            userView.txtUserName.text = user.name

            user.propertyList.forEach { property ->
                val userContainer = LayoutInflater.from(userView.context).inflate(
                    R.layout.select_user_list_item_row_lay,
                    userView.userListContainer,
                    false
                )
                userContainer.txtUserName.text = property.name
                userView.userListContainer.addView(userContainer)
            }
        }
    }
}