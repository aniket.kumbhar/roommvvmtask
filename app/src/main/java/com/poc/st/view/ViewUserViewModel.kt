package com.poc.st.view

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.poc.st.add.repository.db.UserPropertyDao
import com.poc.st.common.User

class ViewUserViewModel(userPropertyDao: UserPropertyDao) : ViewModel() {
    private var _userList: MutableLiveData<List<User>> = MutableLiveData()
    var propertyList: LiveData<List<User>> = _userList

    init {
        _userList.postValue(userPropertyDao.getAllUsersWithSelectedProperties())
    }
}