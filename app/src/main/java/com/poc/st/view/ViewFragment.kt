package com.poc.st.view


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.poc.st.R
import kotlinx.android.synthetic.main.fragment_view.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class ViewFragment : Fragment() {

    private val userPropertyViewModel: ViewUserViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view, container, false)
    }


    companion object {
        @JvmStatic
        fun newInstance() = ViewFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        userPropertyViewModel.propertyList.observe(this, Observer { userList ->

            if (userList.isEmpty()) {
                txtData.visibility = View.VISIBLE
                txtData.text = "No user added yet!!"
            } else {
                listUsers.layoutManager = LinearLayoutManager(context)
                val adapter = UserListAdapter(userList)
                listUsers.adapter = adapter
            }
        })


    }
}
