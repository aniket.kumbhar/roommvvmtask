package com.poc.st

import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.poc.st.add.AddCustomerFragment
import com.poc.st.edit.SelectUserFragment
import com.poc.st.view.ViewFragment
import kotlinx.android.synthetic.main.activity_dashboard.*

class MainActivity : AppCompatActivity() {


    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {


            R.id.navigation_add -> {
                switchFragment(AddCustomerFragment.newInstance())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_view -> {
                switchFragment(ViewFragment.newInstance())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_edit -> {
                switchFragment(SelectUserFragment.newInstance())
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    fun switchFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentContainer, fragment)
            .commit()

            // some changes

            // some another changes
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.activity_dashboard)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        navigation.selectedItemId = R.id.navigation_add
    }


}
