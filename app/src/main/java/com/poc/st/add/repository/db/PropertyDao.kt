package com.poc.st.add.repository.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.poc.st.common.Property

@Dao
abstract class PropertyDao {

    @Insert
    abstract fun insertProprtyList(propertyList: List<Property>)


    @Query("SELECT * FROM PROPERTY WHERE userName =:userName")
    abstract fun getPropertyListByUser(userName: String): MutableList<Property>

    @Update
    abstract fun updatePropertyList(propertyList: List<Property>)

}