package com.poc.st.add.repository.db

import com.poc.st.add.repository.SuccessCallback
import com.poc.st.common.User


/**
 * Interface providing queries from database
 */
class UserPropertyDao(private val userDao: UserDao, private val propertyDao: PropertyDao) {

    // for insertion

    fun insertUserWithProperty(user: User, successCallback: SuccessCallback) {

        try {
            var propertyList = user.propertyList
            for (i in propertyList.indices) {
                propertyList[i].userName = user.name
            }
            propertyDao.insertProprtyList(propertyList)
            userDao.insertUser(user)
            successCallback.invoke(true)
        }catch (e:Exception){
            successCallback.invoke(false)
        }
    }

    // for retrieval
    fun getAllUsersWithProperties(): List<User> {
        val userList: List<User> = userDao.getUser()
        userList.forEach { user ->
            user.propertyList = propertyDao.getPropertyListByUser(user.name)
        }
        return userList
    }


    fun getAllUsersWithSelectedProperties(): List<User> {
        val userList: List<User> = userDao.getUser()
        userList.forEach { user ->
            user.propertyList = propertyDao.getPropertyListByUser(user.name).filter { property ->
                property.isSelected
            }
        }
        return userList
    }

    // for update

    fun updateUserWithProperty(user: User, successCallback: SuccessCallback) {
        try {
            var propertyList = user.propertyList
            for (i in propertyList.indices) {
                propertyList[i].userName = user.name
            }
            propertyDao.updatePropertyList(propertyList)
            userDao.updateUsere(user)
            successCallback.invoke(true)
        }catch (e:Exception){
            successCallback.invoke(false)
        }
    }

    fun checkIfUserAlreadyPresntInDB(name: String) = userDao.isUserAlreadyPresent(name)
}