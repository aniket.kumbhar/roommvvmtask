package com.poc.st.add.repository.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.poc.st.common.Property
import com.poc.st.common.User

/** Room database builder class
 * Returns single instance of App database
 */
@Database(entities = [Property::class, User::class ], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun propertyDao(): PropertyDao

    companion object {
         const val DATABASE_NAME = "propertyDB"
    }
}