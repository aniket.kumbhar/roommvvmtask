package com.poc.st.add.repository.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.poc.st.common.User

@Dao
abstract class UserDao {

    @Insert
    abstract fun insertUser(user: User)

    @Query("SELECT * FROM user WHERE name=:name")
    abstract fun getUserByName(name:String):List<User>

    fun isUserAlreadyPresent(name: String):Boolean = !getUserByName(name).isEmpty()

    @Update
    abstract fun updateUsere(user: User)

    @Query("SELECT * FROM user")
    abstract fun getUser(): List<User>



}