package com.poc.st.add.repository

import com.poc.st.add.repository.db.UserPropertyDao
import com.poc.st.common.Property
import com.poc.st.common.User

/**
 * Repository class to manipulate data from various data sources i.e. network,database
 */


typealias SuccessCallback = (Boolean) -> Unit

class UserPropertyRepository(private val userPropertyDao: UserPropertyDao) {

    fun providePredefinedProperties() = mutableListOf<Property>(
        Property("Magarpatta"),
        Property("Kumar"),
        Property("Nanded City"),
        Property("Lodha"),
        Property("Amit Builder"),
        Property("India bulls properties"),
        Property("Karan builders"),
        Property("Aditya garden city"),
        Property("Nirman Landmark")
    )

    fun storeUserPropertyInDB(user: User,successCallback: SuccessCallback) = userPropertyDao.insertUserWithProperty(user,successCallback)
    fun isUserAlreadyPresnt(name: String) = userPropertyDao.checkIfUserAlreadyPresntInDB(name)
    fun updateUserWithProperty(user: User, successCallback: SuccessCallback) = userPropertyDao.updateUserWithProperty(user,successCallback)
}