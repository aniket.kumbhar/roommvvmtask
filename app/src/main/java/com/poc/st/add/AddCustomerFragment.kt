package com.poc.st.add


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.poc.st.R
import com.poc.st.add.viewmodel.AddUserPropertyViewModel
import com.poc.st.showToast
import kotlinx.android.synthetic.main.fragment_add_customer.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class AddCustomerFragment : Fragment() {


    private val propertyViewModel: AddUserPropertyViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_customer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        bindViewModel()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun bindViewModel() {

        customerNameView.initView(propertyViewModel)
        searchEditText.initView(propertyViewModel)
        propertyViewModel.getStatus().observe(this, Observer { status ->
            when (status) {
                AddUserPropertyViewModel.Status.SUCCESS -> customerNameView.showSuccessEntry()
                AddUserPropertyViewModel.Status.ERROR_ALREADY_PRESENT -> customerNameView.showErrorEntry("Customer already present")
                AddUserPropertyViewModel.Status.ERROR_EMPTY_NAME -> customerNameView.showErrorEntry("Mandatory customer name")
                AddUserPropertyViewModel.Status.ERROR_EMPTY_PROPERTY -> searchEditText.showErrorEntry("Mandatory to select property")
                AddUserPropertyViewModel.Status.SUCCESS_SEARCH_PROPERTY -> searchEditText.showSuccessEntry()

            }
        })

        propertyViewModel.propertyList.observe(this, Observer { propertyList ->

            searchEditText.updateList(propertyList)
        })
        btnAdd.setOnClickListener {
            propertyViewModel.saveUserProperties(
                customerNameView.getCustomerName(),
                searchEditText.getPropertyList()) { success ->
                activity?.let {
                    if (success) {
                        it.showToast("User inserted successfully")
                        customerNameView.resetView()
                        searchEditText.resetView()
                    }else{
                        it.showToast("Sorry. Unable to insert the user")
                    }
                }
            }
        }
    }

    companion object {
        fun newInstance() = AddCustomerFragment()
    }


}
