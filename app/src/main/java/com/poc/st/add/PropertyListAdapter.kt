package com.poc.st.add

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.poc.st.R
import com.poc.st.common.Property
import kotlinx.android.synthetic.main.propery_list_row_item.view.*


class PropertyListAdapter(private val originalPropertyList: List<Property> = mutableListOf()) :
    RecyclerView.Adapter<PropertyListAdapter.PropertyViewHolder>(), Filterable {

    private var filteredList: MutableList<Property> = mutableListOf()
    private val TAG = PropertyListAdapter::class.java.simpleName

    init {
        filteredList = originalPropertyList.toMutableList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PropertyListAdapter.PropertyViewHolder =
        PropertyViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.propery_list_row_item,
                parent,
                false
            )
        )

    override fun getItemCount(): Int = filteredList.size

    override fun onBindViewHolder(holder: PropertyListAdapter.PropertyViewHolder, position: Int) {
        holder.onBind(filteredList[position])
    }

    inner class PropertyViewHolder(var propertyView: View) : RecyclerView.ViewHolder(propertyView) {
        fun onBind(property: Property) {

            propertyView.txtPropertyName.text = property.name
            if (property.isSelected) {
                propertyView.imgSelected.visibility = View.VISIBLE
            } else {
                propertyView.imgSelected.visibility = View.INVISIBLE
            }

            propertyView.setOnClickListener {
                property.isSelected = !property.isSelected
                if (property.isSelected) {
                    propertyView.imgSelected.visibility = View.VISIBLE
                } else {
                    propertyView.imgSelected.visibility = View.INVISIBLE
                }
            }
        }
    }

    lateinit var listEmptyCallback: (Boolean) -> Unit
    fun setEmptyListError(listEmptyCallback : (Boolean)->Unit){
        this.listEmptyCallback = listEmptyCallback
    }


    override fun getFilter(): Filter {

        return object : Filter() {
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                Log.e(TAG, "Publish result : ${constraint.toString()} results :: $results")
                filteredList.clear()
                filteredList.addAll(results?.values as MutableList<Property>)
                if(results.values == null || (results.values as MutableList<Property>).size == 0 ){
                    listEmptyCallback.invoke(true)
                }else{
                    listEmptyCallback.invoke(false)
                }

                notifyDataSetChanged()
            }

            override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {

                val filterResults = Filter.FilterResults()
                if (constraint.isNullOrEmpty()) {
                    filterResults.values = originalPropertyList
                    filterResults.count = originalPropertyList.size
                } else {

                    val filteredPropertyList: MutableList<Property> = mutableListOf()
                    filteredList.forEach {
                        if (it.name.toUpperCase().startsWith(constraint.toString().toUpperCase()))
                            filteredPropertyList.add(it);

                    }
                    filterResults.values = filteredPropertyList
                    filterResults.count = filteredPropertyList.size
                }
                return filterResults
            }
        }

    }

    fun getAdapterList() = filteredList

    fun resetList(){
        filteredList.forEach{
                property -> property.isSelected = false
        }
    }

}