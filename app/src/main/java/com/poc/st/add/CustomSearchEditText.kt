package com.poc.st.add

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.View.OnFocusChangeListener
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.poc.st.R
import com.poc.st.add.viewmodel.AddUserPropertyViewModel
import com.poc.st.common.Property
import com.poc.st.hideKeyboard
import kotlinx.android.synthetic.main.custom_search_edittext.view.*

class CustomSearchEditText @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    private var propertyList: List<Property> = mutableListOf()
    lateinit var adapter: PropertyListAdapter
    lateinit var propertyViewModel: AddUserPropertyViewModel
    val TAG = CustomEditTex::class.java.simpleName
    private var isMandatory = false
    private var canSubmitSearchedValues = false

    init {
        View.inflate(context, R.layout.custom_search_edittext, this)
        setupView(attrs)
    }

    fun initView(propertyViewModel: AddUserPropertyViewModel) {
        this.propertyViewModel = propertyViewModel
    }


    fun setupView(attrs: AttributeSet?) {

        attrs?.let {
            context.obtainStyledAttributes(it, R.styleable.mandatory, 0, 0)?.let { array ->
                isMandatory = array.getBoolean(R.styleable.mandatory_isMandatory, false)
            }
        }

        edtProperty.onFocusChangeListener = OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                showFocusedEntry()
            } else {
                applyValidation()
            }
        }


        edtProperty.addTextChangedListener {
            adapter.filter.filter(it.toString())
        }

        btnDone.setOnClickListener {
            canSubmitSearchedValues = true
            removeFocus()
        }

        btnCancel.setOnClickListener {
            canSubmitSearchedValues = false
            /*showNormalEntry()*/
            propertyViewModel.listCancelButtonClicked()

        }

        if (isMandatory) {
            txtMandatory.visibility = View.VISIBLE
        }
    }

    private fun applyValidation() {
        hideKeyboard()
        if (isMandatory) {
            propertyViewModel.validatePropertyList(getPropertyList())
        } else {
            showNormalEntry()
        }
    }

    fun removeFocus() {
        hideKeyboard()
        edtProperty.clearFocus()
    }


    fun updateList(propertyList: List<Property>) {

        this.propertyList = propertyList
        val propertyLayoutManager = LinearLayoutManager(context)
        listProperties.layoutManager = propertyLayoutManager
        val dividerItemDecoration = DividerItemDecoration(
            listProperties.getContext(),
            propertyLayoutManager.getOrientation()
        )
        listProperties.addItemDecoration(dividerItemDecoration)
        adapter = PropertyListAdapter(propertyList)
        listProperties.adapter = adapter
        adapter.setEmptyListError {
            if (it) {
                txtNoData.visibility = View.VISIBLE
            } else {
                txtNoData.visibility = View.GONE
            }
        }
    }

    fun getPropertyList(): List<Property> {
        Log.e(TAG, "getPropertyList()..")
        if (canSubmitSearchedValues) {
            return adapter.getAdapterList()
        } else {
            return mutableListOf()
        }
    }

    fun showNormalEntry() {
        //views visiblity
        txtFieldName.visibility = View.INVISIBLE
        txtErrorName.visibility = View.GONE
        buttonContainer.visibility = View.INVISIBLE
        listProperties.visibility = View.GONE
        imgValidInvalid.visibility = View.GONE
        edittext_container.setBackgroundResource(R.drawable.edittext_bg_normal)
    }

    fun showFocusedEntry() {
        //views visiblity
        txtFieldName.visibility = View.VISIBLE
        txtErrorName.visibility = View.GONE
        imgValidInvalid.visibility = View.GONE
        listProperties.visibility = View.VISIBLE
        buttonContainer.visibility = View.VISIBLE

        edtProperty.setText("")

        //view colors
        edittext_container.setBackgroundResource(R.drawable.edittext_bg_blue)
        txtFieldName.setTextColor(ContextCompat.getColor(context, R.color.blue))
        txtFieldName.text = edtProperty.hint.toString()
    }

    fun showErrorEntry(message: String) {

        //views visibility
        txtErrorName.visibility = View.VISIBLE
        imgValidInvalid.visibility = View.VISIBLE
        buttonContainer.visibility = View.GONE
        listProperties.visibility = View.GONE

        //view colors
        txtErrorName.setTextColor(ContextCompat.getColor(context, R.color.colorAccent))
        txtFieldName.setTextColor(ContextCompat.getColor(context, R.color.colorAccent))
        edittext_container.setBackgroundResource(R.drawable.edittext_bg_red)

        imgValidInvalid.setImageResource(R.drawable.ic_invalid)
        txtErrorName.text = message
    }


    fun showSuccessEntry() {

        Log.e(TAG, "showSuccessEntry()..")
        txtErrorName.visibility = View.INVISIBLE
        imgValidInvalid.visibility = View.VISIBLE
        buttonContainer.visibility = View.GONE
        listProperties.visibility = View.GONE

        imgValidInvalid.setImageResource(R.drawable.ic_valid)
        txtFieldName.setTextColor(ContextCompat.getColor(context, R.color.grey))
        edittext_container.setBackgroundResource(R.drawable.edittext_bg_normal)
        //asdedtProperty.setText("Multiple")
    }

    fun resetView() {
        //views visiblity
        txtFieldName.visibility = View.INVISIBLE
        txtErrorName.visibility = View.INVISIBLE
        imgValidInvalid.visibility = View.GONE
        buttonContainer.visibility = View.INVISIBLE
        listProperties.visibility = View.GONE
        edittext_container.setBackgroundResource(R.drawable.edittext_bg_normal)
        hideKeyboard()
        adapter.resetList()


    }


}