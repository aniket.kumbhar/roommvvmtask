package com.poc.st.add.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.poc.st.add.repository.SuccessCallback
import com.poc.st.add.repository.UserPropertyRepository
import com.poc.st.common.Property
import com.poc.st.common.User

/**
 * ViewModel class that holds and maintains the data
 */
class AddUserPropertyViewModel(private val repository: UserPropertyRepository) : ViewModel() {

    private var _propertyList: MutableLiveData<List<Property>> = MutableLiveData()
    var propertyList: LiveData<List<Property>> = _propertyList
    val TAG = AddUserPropertyViewModel::class.java.simpleName

    private val status = MutableLiveData<Status>()

    init {
        _propertyList.postValue(repository.providePredefinedProperties())
    }

    fun validateUserName(userName: String): Boolean {

        //TODO optimize this one
        if (userName.isEmpty()) {
            status.value = Status.ERROR_EMPTY_NAME
        } else {
            if (repository.isUserAlreadyPresnt(userName)) {
                status.value = Status.ERROR_ALREADY_PRESENT
            } else {
                status.value = Status.SUCCESS
                return true
            }
        }
        return false
    }

    fun listCancelButtonClicked() {

        status.value = Status.CANCEL_CLICKED
    }

    fun validatePropertyList(propertyList: List<Property>): Boolean = if (propertyList.none(Property::isSelected)) {
        status.value = Status.ERROR_EMPTY_PROPERTY
        false
    } else {
        status.value = Status.SUCCESS_SEARCH_PROPERTY
        true
    }


    fun saveUserProperties(customerName: String, propertyList: List<Property>, successCallback: SuccessCallback) {
        var isUserNameValid = validateUserName(customerName)
        var isPropertyListValid = validatePropertyList(propertyList)
        if (isPropertyListValid && isUserNameValid) {
            repository.storeUserPropertyInDB(
                User(
                    customerName,
                    propertyList
                ), successCallback
            )
        }
    }

    fun updateUserWithProperties(user: User, propertyList: List<Property>, successCallback: SuccessCallback) {
        var isPropertyListValid = validatePropertyList(propertyList)
        if (isPropertyListValid) {
            user.propertyList = propertyList
            repository.updateUserWithProperty(user, successCallback)
        }
    }

    enum class Status {
        SUCCESS,
        SUCCESS_SEARCH_PROPERTY,
        ERROR_ALREADY_PRESENT,
        ERROR_EMPTY_NAME,
        ERROR_EMPTY_PROPERTY,
        CANCEL_CLICKED
    }

    fun getStatus(): LiveData<Status> {
        return status
    }
}