package com.poc.st.add

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.View.OnFocusChangeListener
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.poc.st.R
import com.poc.st.add.viewmodel.AddUserPropertyViewModel
import com.poc.st.hideKeyboard
import kotlinx.android.synthetic.main.custom_edittext_lay.view.*

class CustomEditTex @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    lateinit var propertyViewModel: AddUserPropertyViewModel
    private var isMandatory = false

    init {
        View.inflate(context, R.layout.custom_edittext_lay, this)
        setupView(attrs)
    }

    private fun setupView(attrs: AttributeSet?) {

        attrs?.let {
            context.obtainStyledAttributes(it, R.styleable.mandatory, 0, 0)?.let { array ->
                isMandatory = array.getBoolean(R.styleable.mandatory_isMandatory, false)
            }
        }

        edtCustomerName.onFocusChangeListener = OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                showFocusedEntry()
            } else {
                applyValidation()
            }
        }

        if (isMandatory) {
            txtMandatory.visibility = View.VISIBLE
        }
    }

    private fun applyValidation() {
        hideKeyboard()
        if (isMandatory) {
            propertyViewModel.validateUserName(getCustomerName())
        } else {
            showNormalEntry()
        }
    }

    fun showSuccessEntry() {
        txtErrorName.visibility = View.INVISIBLE
        imgValidInvalid.visibility = View.VISIBLE
        imgValidInvalid.setImageResource(R.drawable.ic_valid)
        txtFieldName.setTextColor(ContextCompat.getColor(context, R.color.grey))
        edittext_container.setBackgroundResource(R.drawable.edittext_bg_normal)
     }

    fun showNormalEntry() {
        //views visiblity
        txtFieldName.visibility = View.INVISIBLE
        txtErrorName.visibility = View.INVISIBLE

        edittext_container.setBackgroundResource(R.drawable.edittext_bg_normal)

    }

    fun resetView() {
        //views visiblity
        txtFieldName.visibility = View.INVISIBLE
        txtErrorName.visibility = View.INVISIBLE
        imgValidInvalid.visibility = View.GONE
        edittext_container.setBackgroundResource(R.drawable.edittext_bg_normal)
        edtCustomerName.setText("")
        hideKeyboard()
    }


    fun showFocusedEntry() {
        //views visiblity
        txtFieldName.visibility = View.VISIBLE
        txtErrorName.visibility = View.INVISIBLE
        imgValidInvalid.visibility = View.GONE

        //view colors
        edittext_container.setBackgroundResource(R.drawable.edittext_bg_blue)
        txtFieldName.setTextColor(ContextCompat.getColor(context, R.color.blue))
        txtFieldName.text = edtCustomerName.hint.toString()
    }

    fun showErrorEntry(message: String) {

        //views visibility
        txtErrorName.visibility = View.VISIBLE
        imgValidInvalid.visibility = View.VISIBLE

        //view colors
        txtErrorName.setTextColor(ContextCompat.getColor(context, R.color.colorAccent))
        txtFieldName.setTextColor(ContextCompat.getColor(context, R.color.colorAccent))
        edittext_container.setBackgroundResource(R.drawable.edittext_bg_red)
        imgValidInvalid.setImageResource(R.drawable.ic_invalid)
        txtErrorName.text = message
    }


    fun initView(propertyViewModel: AddUserPropertyViewModel) {
        this.propertyViewModel = propertyViewModel
    }

    fun getCustomerName() = edtCustomerName.text.toString()

}